#!/bin/bash

set -x

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

cd $1

VERSION=`rpmspec -q --srpm --qf "%{version}" libhangul.spec 2>/dev/null`

if test -d libhangul-$VERSION-build;
then cd libhangul-$VERSION-build;
fi

cd libhangul-$VERSION

autoreconf -f -i -v
./configure --prefix=/usr
check_return_value $?
make
check_return_value $?
make check
exit $?
